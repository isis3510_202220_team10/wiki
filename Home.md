# Home

Team 10
- Daniel M. Beltrán - dm.beltran@uniandes.edu.co - https://gitlab.com/DanBeltranuwu
- Mateo A. Chaparro - ma.chaparro@uniandes.edu.co - https://gitlab.com/MateoCh1
- Laura A. Rodríguez - la.rodriguez@uniandes.edu.co - https://gitlab.com/larodriguez22

## Problems

- [Legal Information](https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2260885803)
- [Hone Food Management](https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2262428026)
- [Pet Health](https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2258716676)
- [Find a Partner to go to transmilenio together](https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2260133955)

## Table Content

0. [Contract](https://gitlab.com/isis3510_202220_team10/wiki/-/blob/main/Contract.md)
1. Sprint 1
    - [MS1](https://gitlab.com/isis3510_202220_team10/wiki/-/blob/main/Sprint%201/MS1.md)
    - [MS2](https://gitlab.com/isis3510_202220_team10/wiki/-/blob/main/Sprint%201/MS2.md)
    - [MS3](https://gitlab.com/isis3510_202220_team10/wiki/-/blob/main/Sprint%201/MS3.md)
    - [MS4](https://gitlab.com/isis3510_202220_team10/wiki/-/blob/main/Sprint%201/MS4.md)

## Add your files



