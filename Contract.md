# Contract
Each of the members of the team pledges to the following terms:
1. Always let the team members know that I've read their messages but at least reacting to them, however responding is preferable
2. Let the team members know ASAP when I'm having any kind of trouble that may stop me from fulfilling my compromises in time in order to organize accordingly
3. Always help a team member in need as long as it is reasonable and feasible, i.e: help with a doubt regarding a concept or with some code error, but not doing a whole task for them, unless the situation justifies it (The team member can't do their part because they got in an accident, not because they were irresponsible and just didn't feel like working)
4. Work ahead of time in order to account for unforseen situations and not have to rush at the end with the deliverable
5. Keep the other members of the group informed regarding my progress with my tasks.
6. Keep the other team members informed about class activities (this helps everyone stay up to date specially when there's a lot of stuff to do)
7. Always commit any advance to my local branch so that if anything happens to me and I can't work my teammates can have the latest version of what I was doing and can finish it much easily
8. Write clear and well documented code so that is easier for the other team members to understand
9. If there are weird merge conflicts I don't really understand contact the person that wrote the part that's generating the conflict to avoid damaging their work.
