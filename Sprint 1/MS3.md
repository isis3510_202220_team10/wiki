# Micro Sprint 3

## Explanation and evidence of the brainstorming process



![ Brainstorming ](resources/brainstorming.png)

At the begining, for brainstorming we met at university in the first floor of the Mario Laserna Building, where we created a Miro and started discussing which idea we liked the most and thought could have a greater audience/impact. We finally settled on the food management problem and so we decided to firstly build the journey map in order to indetify the whole process the interviewee went through, his pain points and positive aspects. After finishing the Journey Map and having a clearer understanding of our user we used the help of Miro's templates and made a brainstorming board in which we first brainstromed general ideas/categories, using the 'yes, and...' methodology, such as the smart fridge or the food planning app putting every idea we could think of, after having these broad categories we followed an inductive idea making process, also using the 'yes, and...' method, in which by having a broad idea we could think of more specific ideas regarding the broad one such as the tracking of food 'usage' in the smart fridge and after a couple of hours of work we had the whole brainstorming board that can be seen in the image above.

<img src="resources/teamwork.jpeg"  width="700">

## The decision process 

![ Decision Process ](resources/brainstorming_votes.jpg)

For the decision process we decided to use the voting methodology in which each of us had 4 votes, we first voted anonymously by each taking a screenshot of the brainstorming board that we had in Miro, so as to not influence each other. Then we showed each other our votes and then we put them in Miro so we could socialize about it. Afterwards we saw that the most voted general idea was the menu calculator app, followed closely by the planning app, we also put two votes into the price comparison app, since after more interviews we felt that prices were one of the biggest pain points of users next to the planning process, which led us to consolidate our solution propositions.

## Empathy maps
- She is Julia Hernández, she is a 63 years old woman, mother head of household, and she lives with her family in Bogotá with a limited amount of money to buy ingredients to their meals. Based on the information provided by Julia in the interview we see that she wants to take care of her finances by finding places where it is more cheap to shop and learn how to make balanced and tasty recipes for her family.
![Empathy map 1](resources/empathy_map_julia.jpg)

- He is Diego Nuñez, he is a 21 years old man, student, and he is living alone in Bogota for the first time without his parents. Based on the information provided by Diego in the interview we see that he wants to spend less time planning meals and is looking to eat healthier and more affordable.
![Empathy map 2](resources/empathy_map_diego.png)

- He is Alvin Gregory, he is a 23 years old man, Co-Founder at Kuppo.co, and he is living alone in Bogota for the first time without his parents. Based on the information provided by Alvin in the interview we see that he tends to skip many meals during the day due to lack of time in his schedule, usually orders prepared food through the rappi app and rarely cooks, making us conclude that Alvin is not a prospective user.
![Empathy map 3](resources/empathy_map_alvin.png)

## Personas Identification


![ Persona 1 ](resources/persona_diego.png)

Diego is representative of people who are 18 to 25 years old, are living for the first time away from home, alone and learning to organize their time, expenses and budget(which for this group is very low), as well as cooking for themselves. This group of people also has high technical skills being able to easily use their phone, install and uninstall apps on it, interact with them even if they are a bit more complex and find all of the app's features most of the times. Finally, due to their low income they have mostly low-end phones and a few have medium-end phones, which would restrict the amount of resources an app has access to.

![ Persona 2 ](resources/persona_julia.png)
Julia is representative of people who are 50 to 70 years old, they have a family who they buy food and cook for, they have limited resources/income, they may have a low education background and they already have cooking experience. This group, due to their age range, isn't really technically skillful, which means, they have some problems using their phones/apps and would need a very clear and easy to use interface. Due to their low income, they also have low-end and a few have medium-end phones, which means any technological solution would have limited resources.
## Solution for the problem

Solution name: **Mikhuna**

Description: It is a Menu Planning/Calculator that will have multiple recipes to choose from with the ingredients needed, but it can also be added personal recipe by adding all of the ingredients with their respective amount to the shopping list. Also users can have a personalized version of recipes they have donend adjust the ingredient amounts so that it better fits their taste and hunger. And they can also upload recipies to share with others and to have their own recipies stored in an accesible place and not have to worry about going through the ingredients when making their shopping list. Each of the the recepies will have different types of diets and suggestions by analyzing the ingredients to be bought and deliver nutritional information that could be relevant to the user according to their desired diet. It also can associate a budget to a shopping list so that the user can keep track of it while buying, and users can save food prices in order to have an estimate for the cost when creating a new shopping list. And to make this process easier, users could upload the prices of what they bought and where they bought it in order for other users to check where it could be cheaper.

![ Solution ](resources/solution_diagram.png)

- **MVP** : The Mikhuna application is designed with a main purpose of meal planning, for which there are two functions, the first: select a recipe and add its ingredients to a shopping list, with the additional features of selecting how many people the dish is for and editing the shopping list at the user's convenience; second: add ingredients for a customized recipe and allow sharing the recipe with other users.
- **Nice to have** : For the Mikhuna application, it would be nice for users to be able to make a price comparison, for which there are two categories: the first, in which users will save the prices they know and will also be able to save in the application a screenshot of invoices, in order to save information; and the second, in which users will save the places they know to shop, in order to save that acquired information. 
- **Premium** : The Premium version of the Mikhuna application, which users will be able to access by paying a subscription, will have three major benefits: first, for users to obtain their selected ingredients delivered directly to their house; second, for users to make purchases directly with Colombian farmers, for which they will need to make a prior reservation; and third, in which employees will update prices of each place to buy food.
