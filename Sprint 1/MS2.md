1. **Problems**
    - Pet Health: https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2258716676

    Description: Keeping track of our pets’ health is sometimes difficult because of all the procedures that are needed to be done at certain times in order for them to have a happy life. This includes deworming externally and internally, vaccination and sterilization. It is easy to forget when it is time to get these procedures done or when to take them for a checkup to the vet.

    - Legal information: https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2260885803

    Description: The average citizen doesn't have much knowledge regarding the different laws in their country which is why in many cases they must resort to lawyers for simple things that end up turning into a big expense for them while the lawyer doesn't need to make much of an effort. Likewise, the law is usually written in a pretty technical and confusing way which makes it even harder for the average citizen to do research on their own. It would be nice if people had access to some sort of app with the basic legal information regarding different cases and explained in a clear way in order to guide them better through the process, help them know what type of legal actions they should take, which legal entities to approach and if it'd be advisable to get a lawyer.

    - Communication for deaf people https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2258719890

    Description: Deaf people often use sign language as a means of communication, but unfortunately, most hearing people do not know it, therefore having conversations becomes more difficult. Additionally, deaf people from all over the world have different sign languages depending on their country of origin, which also creates an additional barrier that does not allow them to express themselves and understand others. 

    - Home Food Management https://uniandes.padlet.org/isis35101/17nn9c1wkaw3twy1/wish/2262428026

    Description: Most of the people that live a very busy life, like employees that work in offices or students, suffer two problems regarding their food at their houses. First, they don´t know what to cook (and take to the office) in a way that is easy and varied. And second, they don't have track of the food they have and the food they need, resulting in many of the food getting expired or missing ingredients at the moment of cooking.

2. **Interviews**
    - Interview template Pet Health:

    Hypothesis: A lot of people forget when they have to take their pets to the veterinary or do a basic procedure

    | Question | Objective |
    | --- | --- |
    | Hello, what is your name and how old are you? | Profiling |
    | Do you have pets? How many and how old are they? What are their names? | Profiling |
    | Have you forgotten any check-ups or routine procedures for your pets? | Gather information for journey map and identify   tool usage |
    | How did you remember? How long did it take you to notice? | Gather information for journey map |
    | What situations help you remember to take your pet to the veterinarian or perform a procedure? | Identify issues |
    | Do you consider the care provided by veterinarians to be effective in emergency situations? | Identify issues |
    | What part of the process do you consider to be quick when you have to perform a basic procedure on your pet? | Identify positive aspects |
    | What would you do if you were faced with the same situation again? | Identify basic facts learned that could help you in the future and if after your experience you think you could perform the process more autonomously |
    | How do you think an application or technological solution could help people like you when they forget a procedure? | Find out what solutions the potential user can see that may not have been initially considered. |

    - Interview template Legal Information:

    Hypothesis:It is difficult for the average citizen to access and understand legal information to solve their problems or doubts 

    | Question | Objective |
    | --- | --- |
    | Hello, what is your name and how old are you? | Profiling |
    | What do you do for a living? | Profiling |
    | How much legal education do you have? | Profiling |
    | Have you ever had any kind of legal problem and if so how did you solve it? | Gather information for journey map - Tool usage |
    | What kind of encounters have you had with Colombian law and how was that process? | Identify situations, issues and positive aspects |
    | What did you like and dislike about the process? | Probing question to identify moore situtations, issues and positive aspects |
    | In what area of the law would you like to be more knowledgeable and why? | Identify points of interest of the interviewee |
    | What do you think is stopping you from doing so? What's the hardest part? | Probing question to identify key issues |
    | How well eduated do you think the average citizen is regarding the law and why? | Asses the interviewee's perspective on the problem/hypothesis |
    | How do you think the law education of the average citizen could be improved? | Asses the interviewee's point of view on solutions |
    | How do you think a digital solution such as an app could help people like you improve their legal eduaction or help them when they find themselves in a legal problem? | Leading question in order to asses the interviewee's point of view on solutions oriented towards technology|

    - Interview Food Management

    Hypothesis: Most of the people that live a very busy life, like students, suffer two problems regarding their food at their houses: planning and preparing the food with a limited amount of budget

    | Question | Objective |
    | --- | --- |
    | Hi, what's your name and how old are you? | Profiling |
    | What do you do? Where are you from? | Profiling |
    | Since you live alone… I would like to know how you organize your meals for the week? Do you prepare your food or do you buy it at the university? | Gather information for journey map and identify tool usage |
    | How do you organize yourself to buy food? how does that make you feel? | Probing question to identify key issues |
    | Can you tell me how it goes when you prepare the meals? |  |
    | Tell me about an experience or how is your day to day cooking? | Gather information for journey map and identify - tool usage |
    | Has it happened to you that a product expires? What have you done about it? | Gather information for journey map and identify - tool usage |
    | Do you know how to organize the meals that are going to be prepared in your house? | Gather information for situations and identify - tool usage |
    | What do you think could be affected by not organizing well what is bought and what is prepared at home? | Gather information for journey map and identify - tool usage |
    | How do you think an application or technological solution could help people like you when planning their meals for the week or month? | Asses the interviewee's point of view on solutions |



3. **Summary/evidence**
    - SUMMARY 

        ![ Interview Summary ](resources/InterviewSummaries.mp3)
        
    - Interview Pet Health 

        ![ Interview Pet Health ](resources/InterviewPetHealth.mp3)

    - Interview Legal Information 

        ![ Interview Legal Information ](resources/InterviewLegalInformation.mp3)

    - Interview Food Management

        ![ Interview Food Management ](resources/audio_food_management.mp3)

4. **Identifying situations**
    - Interview Pet Health
        | Situation | What? | How? | Why? | Who? |
        | --- | --- | --- | --- | --- |
        | 1| Diana's aunt and Diana forgot to vaccinate Gris for a couple of months  | As soon as she noticed the situation, she took Gris to the vet | Diana's aunt was very busy with work and forgot to take him to the vet  | Gris the gray cat, Diana’s aunt and Diana |
        | 2 | A veterinary emergency caused by an infection of the stomach | Inhalation of bacteria from the grass of a public green area | The inhaled bacteria injured the inside walls of his stomach causing vomiting  | Koda a two year old little Westy Terrier |
        | 3 | Koda had to be taken to the veterinarian for a bite from another dog | The dog bit Koda's neck leaving him with a serious injury | Due to the territoriality of both dogs | Koda the little two year old Westy Terrier, a random dog and Diana |
        | 4 | Lack of knowledge of Vero's care routines | Diana realized that she is not aware of Vero's medical care | Because Diana's grandmother is the one who takes care of Vero's medical care. | Vero a ten year old French Puddle and Diana’s grandmother |
        

        
    - Interview Legal Information
        | Situation | What? | How? | Why? | Who? |
        | --- | --- | --- | --- | --- |
        | 1 | Is protesting against the government | Angry because they feel like they are being misstreated | Because they want a change on government policies, however they might have some missunderstandings regarding the law which could negatively impact their goal | The average citizen |
        | 2 | Is trying to learn more about procedural law | Frustrated because she feels she needs more education in order to understand the law because it is very hard to understand since it is written in a very confusing way with too much extra information that is not as relevant | Finds it very relevant in order to know what to do in various situations| The interviewee |
        | 3 | Is being prosecuted because they broke a law | Frustrated and feeling like they're being treated unjustly | They didn't know the law well enough and violated it | The prosecuted |
        | 4 | Is trying to google legal information | Angry and frustrated because where they found it has a costly subscription to access the whole information and it might be outdated | Because she needs to find out some concrete legal information in order to continue with her activities  | The interviewee |


    - Interview Food Management

        | Situation | What? | How? | Why? | Who? |
        | --- | --- | --- | --- | --- |
        | 1 | Juan Jose is thinking about what to prepare for the meals of the next week. | The student looks to be stressed because he/she doesn’t have all the ingredients of the meal he/she was going to prepare and now he/she has to buy something expensive | The student did not calculate well, and he did not buy all the ingredients he needed in the cheap supermarket and now he has to go to the one next door that is more expensive | A student is living alone in a city far away from where he/she grew up |
        | 2 | Juan Jose’s goes on an exchange and is thinking about what to prepare and how to organize his food for the next six months in a way that is healthy and economical. | Excited and a bit scared because he doesn’t have experience cooking for himself. | Juan Jose’s friend doesn’t have the experience, because he has been always with his family where his mom cooks for him all the time | A student that is going for an exchange. |
        | 3 | Juan Jose’s mom is planning the food for the week of her family | His mom is probably feeling analytical | She is trying to optimize the amount of food that she’s going to buy for all her family. | A woman, mom of 3 children who has also a job. |
        | 4 | A student is planning his food for the week | He’s feeling annoyed because that is taking him some time that he could use for doing some other things for the university | The student is very busy at the university and maybe doing other things, so he needs to plan as fast and as good as possible. | A student who doesn’t have time to plan the food for the week |



5.  **Journey map**
    ![ Journey Map ](resources/journeyMap.jpg)
    - Note: Since we are the 3 people group, the teacher said we only needed to do one Journey map, but since it is not written down in the deliverable description we're leaving the comment here so that it can be cross-checked with the teacher while grading in order to avoid having to make a reevaluation request concerning this point. 
6.  **Brain storming**
    ![ Brainstorming ](resources/brainstorming.png)
    - For brainstorming we met at university in the first floor of the Mario Laserna Building, where we created a Miro and started discussing which idea we liked the most and thought could have a greater audience/impact. We finally settled on the food management app and so we decided to firstly build the journey map in order to indetify the whole process the interviewee went through, his pain points and positive aspects. After finishing the Journey Map and having a clearer understanding of our user we used the help of Miro's templates and made a brainstorming board in which we first brainstromed general ideas/categories such as the smart fridge or the food planning app putting every idea we could think of, after having these broad categories we followed an inductive idea making process in which by having a broad idea we could think of more specific ideas regarding the broad one such as the tracking of food 'usage' in the smart fridge and after a couple of hours of work we had the whole brainstorming board that can be seen in the image above.  
    ![ Brainstorming ](resources/teamwork.jpeg)
    - Photo of the team during the brainstorming session. 
7. **Prototype**
    - This prototype was a product of selecting the ideas we liked the best out of all of the ones produced during the brainstorming process and synthetizing them into one solid proposition, we then pondered and discussed for a short while on what name we should give to it and how to do the prototype which we ended up doing through a body-storming process in which we made a sort of basic "ad" in which the listener could find out about the core functionalities the solution would have in order to solve the core problems we identified. This would let us show a basic concept to the user and find out wether the proposed solution solves the the core problems we identified and if they really are core problems or if we should focus in other ones that are more relevant to the user. 
    https://www.canva.com/design/DAFJ1F2qnFQ/3Jf_WQhG039W73FZB4FL5w/watch?utm_content=DAFJ1F2qnFQ&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton
    
